﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Application.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Application.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Application.Services
{
    public class PartnerServiceTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly IPartnerService _partnerService;

        public PartnerServiceTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnerService = fixture.Build<PartnerService>().Create();
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }
        
        [Fact]
        public async void CancelPartnerPromoCodeLimitAsync_PartnerIsNotFound_ThrowsEntityNotFoundException()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            Func<Task> act = async () => await _partnerService.CancelPartnerPromoCodeLimitAsync(partnerId);

            // Assert
            await act.Should().ThrowAsync<EntityNotFoundException>("Данный партнер не активен");
        }
        
        [Fact]
        public async void CancelPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.IsActive = false;
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            Func<Task> act = async () => await _partnerService.CancelPartnerPromoCodeLimitAsync(partnerId);

            // Assert
            await act.Should().ThrowAsync<ChangePartnerLimitException>("Лимит должен быть больше 0");
        }
    }
}